package com.cogastro.ms;

public class Api {
    private ApiClassificators apiClassificators;
    private ApiOperations apiOperations;

    public ApiClassificators classificator() {
        if (apiClassificators == null)
            apiClassificators = new ApiClassificators();
        return apiClassificators;
    }
    public ApiOperations operations()
    {
        if (apiOperations==null)
        {
            apiOperations=new ApiOperations();
        }
        return apiOperations;
    }
}
