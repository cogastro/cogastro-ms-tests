package com.cogastro.ms;

import com.cogastro.ms.data.generator.classificators.api.CarryingUnitControllerApi;
import com.cogastro.ms.data.generator.classificators.api.FeedTypeControllerApi;
import com.cogastro.ms.data.generator.classificators.api.StagesControllerApi;
import com.cogastro.ms.data.generator.models.*;
import com.cogastro.ms.data.generator.operations.api.BatchControllerApi;

public class ApiClassificators {
    private static String BASE_PATH_CLASSIFICATORS = "http://platform.cogastro.com/api/classificators/";

    private BatchControllerApi batchControllerApi = new BatchControllerApi();
    private FeedTypeControllerApi feedTypeApi = new FeedTypeControllerApi();
    private CarryingUnitControllerApi carryingUnitApi=new CarryingUnitControllerApi();
    private StagesControllerApi stageApi=new StagesControllerApi();

    public ApiClassificators()
    {
        feedTypeApi.getApiClient().setBasePath(BASE_PATH_CLASSIFICATORS);
        carryingUnitApi.getApiClient().setBasePath(BASE_PATH_CLASSIFICATORS);
        stageApi.getApiClient().setBasePath(BASE_PATH_CLASSIFICATORS);
    }

    public CarryingUnit createCarryingUnit(String name)
    {
        NewCarryingUnit newCarryingUnit=new NewCarryingUnit();
        newCarryingUnit.setName(name);
        newCarryingUnit.setWeight(455);
        newCarryingUnit.setLength(600);
        newCarryingUnit.setWidth(400);
        newCarryingUnit.setHeight(100);
        return carryingUnitApi.createUsingPOST(newCarryingUnit);
    }
    public Stage createStage(String name)
    {
        NewStage stage=stageApi.getSuggestedSequenceNumberUsingGET();
        stage.setName(name);
        return stageApi.createUsingPOST2(stage);
    }
    public FeedType createFeedType(String name)
    {
        NewFeedType newFeedType = new NewFeedType();
        newFeedType.name(name);

        return feedTypeApi.createUsingPOST1(newFeedType);
    }
}
