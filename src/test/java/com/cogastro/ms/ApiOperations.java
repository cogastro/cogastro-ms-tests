package com.cogastro.ms;

import com.cogastro.ms.data.generator.models.*;
import com.cogastro.ms.data.generator.operations.api.BatchControllerApi;
import com.cogastro.ms.data.generator.operations.api.BatchStageControllerApi;
import com.cogastro.ms.data.generator.operations.api.CleaningControllerApi;
import com.cogastro.ms.data.generator.operations.api.FeedingControllerApi;
import org.joda.time.DateTime;

import java.time.OffsetDateTime;
import java.util.Date;


public class ApiOperations {
    private static String BASE_PATH_OPERATIONS = "http://platform.cogastro.com/api/app-operations/";
    private BatchControllerApi batchControllerApi=new BatchControllerApi();
    private BatchStageControllerApi batchStageControllerApi=new BatchStageControllerApi();
    private FeedingControllerApi feedingControllerApi=new FeedingControllerApi();
    private CleaningControllerApi cleaningControllerApi=new CleaningControllerApi();

    public ApiOperations() {
        batchControllerApi.getApiClient().setBasePath(BASE_PATH_OPERATIONS);
        batchStageControllerApi.getApiClient().setBasePath(BASE_PATH_OPERATIONS);
        feedingControllerApi.getApiClient().setBasePath(BASE_PATH_OPERATIONS);
        cleaningControllerApi.getApiClient().setBasePath(BASE_PATH_OPERATIONS);
    }
    public Batch createBatch(String name,int weight, CarryingUnit carryingUnit,Stage stage)
    {
        NewBatch newBatch=new NewBatch();
        newBatch.setCarryingUnit(carryingUnit);
        newBatch.setCurrentStage(stage);
        newBatch.setTagId(name);
        newBatch.setName(name);
        newBatch.setInitialWeight(weight);
        newBatch.setRegistrationDate(DateTime.now().toDate());
        newBatch.setEmanationDate(DateTime.now().toDate());
        return batchControllerApi.createUsingPOST(newBatch);
    }
    public Feeding createFeeding(Batch batch,FeedType feedType,int weight)
    {
        NewFeeding newFeeding=new NewFeeding();
        newFeeding.setFeedType(feedType);
        newFeeding.setFeedWeight(weight);
        return feedingControllerApi.createUsingPOST3(batch.getId(),newFeeding);
    }
    public Cleaning createCleaning(Batch batch,FeedType feedType,int weight) {
        NewCleaning newCleaning = new NewCleaning();
        newCleaning.feedType(feedType);
        newCleaning.setWeight(weight);
        return cleaningControllerApi.createUsingPOST2(batch.getId(),newCleaning);
    }
}
