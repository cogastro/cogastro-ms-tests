package com.cogastro.ms;


import com.cogastro.ms.data.generator.models.Batch;
import com.cogastro.ms.data.generator.models.CarryingUnit;
import com.cogastro.ms.data.generator.models.FeedType;
import com.cogastro.ms.data.generator.models.Stage;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.HttpClientErrorException;


public class GenerateData extends Api {

    @Before
    public void init() {


    }

    @Test
    public void createBatch() throws Exception {
        try {
            CarryingUnit carryingUnit = this.classificator().createCarryingUnit("Back box");
            FeedType feedType = this.classificator().createFeedType("Morka");
            Stage stage = this.classificator().createStage("initial");
            Batch batch = this.operations().createBatch("00001", 223, carryingUnit, stage);
            for (int i = 1; i <= 10; i++) {

                this.operations().createFeeding(batch, feedType, 54);
                this.operations().createCleaning(batch, feedType, 23);
            }
        } catch (HttpClientErrorException ex) {
            System.out.println(ex.getResponseBodyAsString());
            throw ex;
        }
    }


}
